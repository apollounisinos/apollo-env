package apollo.mota.unisinos.br.apollo2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class ApolloClientWS {

    public void sendData(String ambienteID, String iluminacao, String temperatura, String datahora) {
        String url = "http://192.168.43.253:8080/ApolloWS/monitoramentoAmbiente/persist";
        URL urlConnection;
        HttpURLConnection connection = null;
        try {
            try {
                urlConnection = new URL(url);
                connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("ambiente_id", ambienteID);
                connection.setRequestProperty("temperatura", temperatura);
                connection.setRequestProperty("iluminacao", iluminacao);
                connection.setRequestProperty("datahora", datahora);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write("");
                out.close();

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
//                        Fila temp = gson.fromJson(line, Fila.class);
//                        fila.setId(temp.getId());
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startSending() {
        Thread thread = new Thread(){
            public void run(){
                while (true) {
                    try {
                        String temperature = new YahooWSClient().retrieveTemperature();
                        temperature = new FormatterUtil().extractTemperature(temperature).replace("\"", "");
                        String datahora = new FormatterUtil().formatDateToString(new Date());
                        MainActivity.temperature = temperature;
                        System.out.println("temp: " + temperature);
                        System.out.println("datahora: " + datahora);
                        System.out.println("iluminacao: " + MainActivity.brightness);
                        new SensorTask().doInBackground(MainActivity.brightness + "", temperature, datahora);
                        Thread.sleep(1000);
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

}
