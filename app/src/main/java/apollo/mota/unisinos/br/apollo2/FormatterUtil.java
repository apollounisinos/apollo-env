package apollo.mota.unisinos.br.apollo2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatterUtil {

    private final DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public String extractTemperature(String json) {
        int indexTemp = json.indexOf("temp");
        System.out.println(indexTemp);
        return json.substring(indexTemp + 7, indexTemp + 9);
    }

    public String formatDateToString(Date date) {
        try {
            return format.format(date);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
