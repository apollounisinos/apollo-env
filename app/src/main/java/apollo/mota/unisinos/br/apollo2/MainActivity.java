package apollo.mota.unisinos.br.apollo2;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorAccelerometer;
    public static float brightness;
    public static String temperature = "---";
    private boolean ledHelper = false;

    TextView light, helperDescription;
    Switch helper;
    public static TextView temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Apollo EFP Env");
        setContentView(R.layout.activity_main);

        //Define components
        light = findViewById(R.id.light_value);
        temp = findViewById(R.id.temperature_value);
        helper = findViewById(R.id.switch1);
        helperDescription = findViewById(R.id.helper_value);

        //Get data from accelerometer
        sensorAccelerometer = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensorAccelerometer.registerListener(this,
                sensorAccelerometer.getDefaultSensor(Sensor.TYPE_LIGHT),
                SensorManager.SENSOR_DELAY_NORMAL);

        //LED Helper
        helper.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ledHelper = true;
                } else {
                    ledHelper = false;
                }
            }
        });

        //Start thread
        new ApolloClientWS().startSending();
        updateTemperaure();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy){
    }

    public void updateTemperaure() {
        Thread thread = new Thread(){
            public void run(){
                while (true) {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                temp.setText(temperature + "c");
                            }
                        });
                        Thread.sleep(1000);
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public void onSensorChanged(SensorEvent event) {
        //Use the information to show on activity and send through ws
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            //Save data
            brightness = event.values[0];

            //Show it
            light.setText(brightness + "");

            if (ledHelper) {
                if (brightness < 50) {
                    try {
                        CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                        String cameraId = camManager.getCameraIdList()[0]; // Usually front camera is at 0 position.
                        camManager.setTorchMode(cameraId, true);
                        helperDescription.setText("ligado");
                    } catch (Exception e) {
                        System.out.println("Fail turning on the led" + e.getMessage());
                    }
                } else {
                    try {
                        CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                        String cameraId = camManager.getCameraIdList()[0]; // Usually front camera is at 0 position.
                        camManager.setTorchMode(cameraId, false);
                        helperDescription.setText("desligado");
                    } catch (Exception e) {
                        System.out.println("Fail turning off the led");
                    }
                }
            }
        }
    }
}
